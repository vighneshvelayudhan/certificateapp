const fs = require("fs");

const keyPairs = [
  [
    "0x9b7b3143782861277ef38a77386ee79551b82615c0a47d7f3ecafcbd9c2b7d23",
    "0xedfc0d852ccb4e790125cf2891d65970f2ac00a4",
  ], //- 0.2 matic
  [
    "0x60dd6b72f2918b8f8b8338e944e04c0b01bba533b09f9f95c161644a926a5ba0",
    "0x0ac2c7059cedd945a13073a7eb441c36e35ccf18",
  ], // - 0.2 matic
  [
    "0x8b378363be4d35ccdd336d8b7a176892bc6132a27b2905ca7245cb73a127b313",
    "0xf267735fd551a1dc341ac7a3227cacc312f7dfb7",
  ],
  [
    "0xcadec88ff58c93f1bbafdd565f7a2207abd6e71b4296ec7d57308c1a5b3dd5b0",
    "0xa454a05d3989f55ff2fc22052e61a6a1911209b0",
  ],
  [
    "0xe4c60df83ed4263697b8e6ef1432cbde9566f64915f8c7bd66bc5d476e8f5e50",
    "0xe49b2820fa0b3a5f10b4cb40f836a0e85dc6eaef",
  ],
];

let resString = "";
keyPairs.forEach((keyPair) => {
  const pubkey = keyPair[1].replace("0x", "");
  const privateKey = keyPair[0].replace("0x", "");
  resString += `node index.js addIssuer --pubkey "${pubkey}" --name IITB\n`;
  resString += `node index.js addCertificate --pubkey "${pubkey}" --privatekey "${privateKey}" --file "cert1.pdf" --certnum 001 --name IITB\n`;
  resString += `node index.js verifyCertificate --file "Signed_cert1.pdf"\n`;
  resString += `node index.js revokeCertificate --file "Signed_cert1.pdf" --privateKey "${privateKey}" --reason "student broke code of conduct"\n`;
  resString += `node index.js verifyCertificate --file "Signed_cert1.pdf"\n\n\n`;
});

fs.writeFileSync("usage_filled.txt", resString);
