node index.js addIssuer --pubkey "a4095851bfe4a370d91d9a0b815269b0c13f56a2" --name IITB
node index.js addCertificate --pubkey "a4095851bfe4a370d91d9a0b815269b0c13f56a2" --privatekey "589971200bed51cbff84c5b122b92313a753a916d62663e5549ab866f51096ac" --file "cert1.pdf" --certnum 001 --name IITB
node index.js verifyCertificate --file "Signed_cert1.pdf"
node index.js revokeCertificate --file "Signed_cert1.pdf" --privateKey "589971200bed51cbff84c5b122b92313a753a916d62663e5549ab866f51096ac" --reason "student broke code of conduct"
node index.js verifyCertificate --file "Signed_cert1.pdf"


