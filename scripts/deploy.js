const hre = require("hardhat");
const fs = require("fs");
async function main() {
  try {
    const certContractFactory = await hre.ethers.getContractFactory("CertApp");
    const certContract = await certContractFactory.deploy();
    console.log("Certificate Contract Deployed");
    const contracts = {
      certContract: certContract.address,
    };
    let data = JSON.stringify(contracts);
    fs.writeFileSync("contract_details.json", data);
  } catch (err) {
    console.log(err);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
