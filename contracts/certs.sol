// SPDX-License-Identifier: MIT
pragma solidity ^0.8;

contract CertApp {
    struct certificate {
        bytes32 certHash;
        uint256 expiry;
        string revokeReason;
    }
    struct issuerCertMap {
        mapping(uint32 => certificate) certMap;
    }
    mapping(address => string) issuers;
    mapping(address => issuerCertMap) certificates;

    address owner;

    constructor() {
        owner = msg.sender;
    }

    /*
        String compare using 
        https://github.com/ethereum/dapp-bin/blob/master/library/stringUtils.sol

    */
    function compare(string memory _a, string memory _b)
        private
        pure
        returns (int256)
    {
        bytes memory a = bytes(_a);
        bytes memory b = bytes(_b);
        uint256 minLength = a.length;
        if (b.length < minLength) minLength = b.length;
        //@todo unroll the loop into increments of 32 and do full 32 byte comparisons
        for (uint256 i = 0; i < minLength; i++)
            if (a[i] < b[i]) return -1;
            else if (a[i] > b[i]) return 1;
        if (a.length < b.length) return -1;
        else if (a.length > b.length) return 1;
        else return 0;
    }

    function addIssuer(address issuer, string memory name) public {
        require(msg.sender == owner, "E01");
        require(compare(issuers[issuer], "") == 0, "E02");
        issuers[issuer] = name;
    }

    function addCertificate(
        bytes32 certHash,
        uint32 certId,
        uint256 expiry
    ) public {
        // issuer should be valid
        require(compare(issuers[msg.sender], "") != 0, "E02");
        // certificate should not be issued
        require(certificates[msg.sender].certMap[certId].expiry == 0, "E03");
        // @todo add more validators
        certificates[msg.sender].certMap[certId].certHash = certHash;
        certificates[msg.sender].certMap[certId].expiry =
            block.timestamp +
            expiry *
            1 minutes;
    }

    function revokeCertificate(uint32 certId, string memory reason) public {
        // issuer should be valid
        require(compare(issuers[msg.sender], "") != 0, "E02");
        // certificate should be present and not already revoked
        require(certificates[msg.sender].certMap[certId].expiry != 0, "E04");

        certificates[msg.sender].certMap[certId].revokeReason = reason;
        certificates[msg.sender].certMap[certId].expiry = 0;
    }

    function validateCertificate(
        address issuer,
        uint32 certId,
        bytes32 certHash
    ) public view returns (bool) {
        if (compare(issuers[issuer], "") == 0) {
            return false;
        }

        if (certificates[issuer].certMap[certId].expiry < block.timestamp) {
            return false;
        }
        if (certificates[issuer].certMap[certId].certHash != certHash) {
            return false;
        }
        return true;
    }

    function viewCertificate(
        address issuer,
        uint32 certId,
        bytes32 certHash
    ) public view returns (string memory) {
        require(compare(issuers[issuer], "") != 0, "E02");
        int reasonCompare = compare(
            certificates[issuer].certMap[certId].revokeReason,
            ""
        );
        if (certificates[issuer].certMap[certId].expiry == 0) {
            require(reasonCompare != 0, "E05");
        }
        require(
            certificates[issuer].certMap[certId].certHash == certHash,
            "E06"
        );

        if (reasonCompare != 0)
            return certificates[issuer].certMap[certId].revokeReason;
        else if (certificates[issuer].certMap[certId].expiry < block.timestamp)
            return "expired";
        else if (certificates[issuer].certMap[certId].certHash != certHash)
            return "invalid";
        return "valid";
    }

    function validateIssuer(address issuer) public view returns (bool) {
        if (compare(issuers[issuer], "") == 0) {
            return false;
        }

        return true;
    }
}
